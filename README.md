### Opencart2 : ViaBill ePay Module ### 
----------------------  

ViaBill has developed a free payment module using ePay Payment Gateway which enables your customers to pay online for their orders in your Opencart Web Shop.
ViaBill is a Payment Method and not a Payment Gateway.

###Facts###
-----------------
- version: 1.1
- Plugin on BitBucket (https://bitbucket.org/ibilldev/viabill-epay-opencart2-plugin/)


###Description###
-----------
Pay using ViaBill. 
Install this Plugin in to your Opencart2 Web Shop to provide a separate payment option to pay using ViaBill.

#Requirements
------------
* PHP >= 5.2.0

#Compatibility
-------------
* Opencart 2


###Integration Instructions###
-------------------------
1. Download the Module from the bitbucket. 

2. Module contains two folder a.) admin b.) catalog 

Please follow the following folder structure to place the relevant files for ViaBill ePay Integration:


![opencart-epay-admin.PNG](https://bitbucket.org/repo/gpqend/images/2216712145-opencart-epay-admin.PNG)
![opencart-epay-catalog.PNG](https://bitbucket.org/repo/gpqend/images/2747742551-opencart-epay-catalog.PNG)


3. Copy the content of admin folder inside your project's admin folder(Follow the folder structure)

4. Copy the contents of catalog folder inside your project's cataog folder(Follow the folder structure)

5. Go to the Admin-> Extensions-> Payments. Look for "ViaBill ePay Payment Solutions"

6. Click on Install.

7. Now edit on the plugin.

8. Enable/Disable :  Please Enable the Status. 

9. Enter Merchant ID/Agreement ID/ API Key/ Private Key/Language

10. Save and Done.


##Uninstallation/Disable Module
-----------------------
1. Go to the Admin->Extensions->Payments
2. Look For "ViaBill ePay Payment Solutions"
3. Choose Option to Uninstall


#Support
-------
If you have any issues with this extension, kindly drop us a mail on [support@viabill.com](mailto:support@viabill.com)

#Contribution
------------


#License
-------
[OSL - Open Software Licence 3.0](http://opensource.org/licenses/osl-3.0.php)
